help: ##
	@echo ""
	@echo "Usage:  make COMMAND"
	@echo ""
	@echo "Commands:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
.PHONY: help

build:        ## Build docker containers for local development
	@echo "Build all the services"
	@echo ""
	@cd ${PWD}/core-service && docker compose build && cd ..
	@cd ${PWD}/auth-service && docker compose build && cd ..
	@cd ${PWD}/api-service && docker compose build && cd ..
	@echo ""
	@echo "Done"
.PHONY: build

build-hard:   ## Build (without cache) docker containers for local development
	@echo "Build all the services"
	@echo ""
	@cd ${PWD}/core-service && docker compose build --no-cache && cd ..
	@cd ${PWD}/auth-service && docker compose build --no-cache && cd ..
	@cd ${PWD}/api-service && docker compose build --no-cache && cd ..
	@echo ""
	@echo "Done"
.PHONY: build-hard

build-up:     ## Build and go up with docker containers for local development
	@echo "Build all the services"
	@echo ""
	@cd ${PWD}/core-service && docker compose build && cd ..
	@cd ${PWD}/auth-service && docker compose build && cd ..
	@cd ${PWD}/api-service && docker compose build && cd ..
	@cd ${PWD}/core-service && docker compose up -d && cd ..
	@cd ${PWD}/auth-service && docker compose up -d && cd ..
	@cd ${PWD}/api-service && docker compose up -d && cd ..
	@echo ""
	@echo "Done"
.PHONY: build-up

up:           ## Up docker containers for local development
	@echo "Put up all the services"
	@echo ""
	@cd ${PWD}/core-service && docker compose up -d && cd ..
	@cd ${PWD}/auth-service && docker compose up -d && cd ..
	@cd ${PWD}/api-service && docker compose up -d && cd ..
.PHONY: up

down:         ## Down docker containers for local development
	@echo "Put down all the services"
	@echo ""
	@cd ${PWD}/api-service && docker compose down && cd ..
	@cd ${PWD}/auth-service && docker compose down && cd ..
	@cd ${PWD}/core-service && docker compose down && cd ..
	@echo ""
	@echo "Done"
.PHONY: down

start:        ## Start docker containers for local development
	@echo "Start all the services"
	@echo ""
	@cd ${PWD}/core-service && docker compose start && cd ..
	@cd ${PWD}/auth-service && docker compose start && cd ..
	@cd ${PWD}/api-service && docker compose start && cd ..
	@echo ""
	@echo "Done"
.PHONY: core-start

stop:         ## Stop docker containers for local development
	@echo "Stop all the services"
	@echo ""
	@cd ${PWD}/core-service && docker compose stop && cd ..
	@cd ${PWD}/auth-service && docker compose stop && cd ..
	@cd ${PWD}/api-service && docker compose stop && cd ..
	@echo ""
	@echo "Done"
.PHONY: stop

rebuild-core: ## Rebuild core service
	@echo "Rebuild core service"
	@echo ""
	@cd ${PWD}/core-service && docker compose down && docker compose build && docker compose up -d && cd ..
	@echo ""
	@echo "Done"
.PHONY: rebuild-core

rebuild-auth: ## Rebuild auth service
	@echo "Rebuild auth service"
	@echo ""
	@cd ${PWD}/auth-service && docker compose down && docker compose build && docker compose up -d && cd ..
	@echo ""
	@echo "Done"
.PHONY: rebuild-auth

rebuild-api:  ## Rebuild api service
	@echo "Rebuild api service"
	@echo ""
	@cd ${PWD}/api-service && docker compose down && docker compose build && docker compose up -d && cd ..
	@echo ""
	@echo "Done"
.PHONY: rebuild-api

rebuild-all:  ## Rebuild all services
	@echo "Rebuild all services"
	@echo ""
	@cd ${PWD}/api-service && docker compose down && cd ..
	@cd ${PWD}/auth-service && docker compose down && cd ..
	@cd ${PWD}/core-service && docker compose down && cd ..
	@cd ${PWD}/core-service && docker compose build && docker compose up -d && cd ..
	@cd ${PWD}/auth-service && docker compose build && docker compose up -d && cd ..
	@cd ${PWD}/api-service && docker compose build && docker compose up -d && cd ..
	@echo ""
	@echo "Done"
.PHONY: rebuild-all
