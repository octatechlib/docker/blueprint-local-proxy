# Blueprint Local Proxy
[![State badge](https://img.shields.io/badge/Build-no_build-orange.svg)](https://gitlab.com/octatechlib/docker/blueprint-local-proxy)
[![Version badge](https://img.shields.io/badge/Version-1.0-blue.svg)](https://gitlab.com/octatechlib/docker/blueprint-local-proxy/-/tree/1.0?ref_type=tags)
[![Last stable badge](https://img.shields.io/badge/Last_stable-1.0-green.svg)](https://gitlab.com/octatechlib/docker/blueprint-local-proxy/-/tree/1.0?ref_type=tags)
[![Licence badge](https://img.shields.io/badge/Licence-GPL-1f425f.svg)](https://gitlab.com/octatechlib/docker/blueprint-local-proxy)

Separated per type plus shared serviced config skeleton for multiple-container based structure.

## Getting started

This is a blueprint for basic docker based configuration. Two web services separated per own container, plus shared services. PHP version specified in Dockerfile. Adjust to your needs. Pay attention to nginx configs.

#### Must know
Meant for MacOS. Other ymls soon.

### Contains
- core config (mutual services that both auth and api will use)
- api service config
- auth service config

### Example


![example](./example.png)

## License
[![License](https://img.shields.io/badge/License-GPL_2.0-blue.svg)](https://opensource.org/license/gpl-2-0/)

## Project status
In progress.