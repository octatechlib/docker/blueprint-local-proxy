# Authentication Service
### Details

[![Licence badge](https://img.shields.io/badge/Licence-GPL-1f425f.svg)](https://gitlab.com/octatechlib/docker/blueprint-local-proxy)

## Connecting to external network

#### Project 1
Main project example

```yaml
version: '3.8'

volumes:
    mysql_data_auth:

networks:
  proxy_core_network:
    driver: bridge
    name: proxy_core_network
    ipam:
      config:
        - subnet: 200.17.0.0/16
          gateway: 200.17.0.1

services:
    project1_mysql:
        # ...
        networks:
          - default
          - proxy_core_network

    project1_app1:
        # ...
        networks:
            - default
            - proxy_core_network

    project1_app2:
        # ...
        networks:
          - default
          - proxy_core_network

```

#### Project 2
Projects connecting to this network example:
```yaml
version: '3.8'

volumes:
    mysql_data_proxy_core:

networks:
  proxy_core_network:
    external: true

services:
    project2_app1:
        # ...
        networks:
          - default
          - proxy_core_network

```
