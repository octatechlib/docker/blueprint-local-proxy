# Core Service

<p>
    This project should be used for local services that can be shared over local docker services.
</p>

| Project                | Address                     |
|------------------------|---------------------------------------------------------------|
| Authentication Service | [auth.proxy-core.local](http://auth.proxy-core.local/)        |
| Mailhog                | [mail.proxy-core.local](http://mail.proxy-core.local/)        |
| API Service            | [api.proxy-core.local](http://api.proxy-core.local/)          |
|------------------------|---------------------------------------------------------------|

## Getting started

```shell
make up
```

## Hosts
/etc/hosts
```bash
200.17.0.1	proxy-core.local auth.proxy-core.local mail.proxy-core.local api.proxy-core.local
```

## Add the network to your docker-compose services

```yml
networks:
  proxy_core_network:
    external: true

services:
  some_service:
    networks:
      - default
      - proxy_core_network
```

## Configure nginx

### Add the nginx environment variables to the docker service
docker.compose.yml
```yml
environment:
  - VIRTUAL_HOST=api.proxy-core.local
  - VIRTUAL_PORT=8025
```

## Add the laravel environment variables

```env
DB_HOST=mysql_proxy_core
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=somepass

MAIL_HOST=mailhog_proxy_core
MAIL_PORT=1025
MAIL_FROM_ADDRESS=no-reply@my-website.com
```

## Mailhog

[Mailhog url](http://localhost:8025)
